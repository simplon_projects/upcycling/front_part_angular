import { TrashObject } from "./trashObject";
import { User } from "./user";

export interface UpcycledObject  {
    id?:number;
    name?:string;
    description?:string;
    picturePath?:string;
    craftedBy:User;
    oldVersion:TrashObject;
}
