import { User } from "./user";
import { Category } from "./category";

export interface TrashObject {
    id?:number;
    name?:string;
    description?:string;
    picturePath?:string;
    droppedBy?:User;
    category?:Category;
}