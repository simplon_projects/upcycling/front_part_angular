import { TrashObject } from "./trashObject";
import { UpcycledObject } from "./upcycledObject";

export interface User {
    id?:number;
    username?:string;
    email:string;
    password?:string;
    roles?:string[];
    lastName?:string;
    firstName?:string;
    adress?:string;
    postalCode?:number;
    city?:string;
    avatarPath?:string;
    trashObjects?:Array<TrashObject>;
    upcycledObject?:Array<UpcycledObject>;
}
