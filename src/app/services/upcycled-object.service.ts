import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UpcycledObject } from 'app/entity/upcycledObject';

@Injectable({
  providedIn: 'root'
})
export class UpcycledObjectService {

  constructor(private http:HttpClient) { }

  getAll(): Observable<UpcycledObject[]> {
    return this.http.get<UpcycledObject[]>('http://localhost:8000//api/upcycledobject');
  }
}
