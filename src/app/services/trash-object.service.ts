import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TrashObject } from 'app/entity/trashObject';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TrashObjectService {


  constructor(private http:HttpClient) { }

  getAll(): Observable<TrashObject[]> {
    return this.http.get<TrashObject[]>('http://localhost:8000/api/trashobject');
  }

  getOne(id: number): Observable<TrashObject> {
    return this.http.get<TrashObject>('http://localhost:8000/api/trashobject/' + id);
  }
}
