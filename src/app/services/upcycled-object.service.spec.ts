import { TestBed } from '@angular/core/testing';

import { UpcycledObjectService } from './upcycled-object.service';

describe('UpcycledObjectService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UpcycledObjectService = TestBed.get(UpcycledObjectService);
    expect(service).toBeTruthy();
  });
});
