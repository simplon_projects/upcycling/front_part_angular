import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { UserComponent } from './user/user.component';
import { AboutComponent } from './pages/about/about.component';
import { TrashObjectComponent } from './pages/trash-object/trash-object.component';
import { UpcycledObjectComponent } from './pages/upcycled-object/upcycled-object.component';
import { DetailsComponent } from './pages/trash-object/details/details.component';

const routes: Routes =[
    { path:'', component: HomeComponent},
    { path:'login', component: LoginComponent },
    { path:'register', component: RegisterComponent},
    { path:'user', component: UserComponent},
    { path:'about', component:AboutComponent},
    { path:'products', component:TrashObjectComponent},
    { path:'upcycled', component:UpcycledObjectComponent},
    { path:'details/:id', component:DetailsComponent}
    ];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
