import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navigation/navbar/navbar.component';
import { FooterComponent } from './navigation/footer/footer.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './login/login.component';

import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { JwtModule } from "@auth0/angular-jwt";
import { JwtService } from './interceptor/jwt.service';
import { RegisterComponent } from './register/register.component';
import { AboutComponent } from './pages/about/about.component';
import { TrashObjectComponent } from './pages/trash-object/trash-object.component';
import { UpcycledObjectComponent } from './pages/upcycled-object/upcycled-object.component';
import { UserComponent } from './user/user.component';
import { DetailsComponent } from './pages/trash-object/details/details.component';


export function getToken() {
  return localStorage.getItem('token');
}

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    AboutComponent,
    TrashObjectComponent,
    UpcycledObjectComponent,
    UserComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    NgbModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: getToken,
        whitelistedDomains: ['localhost:8000'],
        skipWhenExpired: true
      }
    })
  ],
  providers: [{provide: HTTP_INTERCEPTORS, useClass: JwtService, multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
