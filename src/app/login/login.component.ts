import { Component, OnInit, Output } from '@angular/core';
import { User } from 'app/entity/user';
import { AuthService } from 'app/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  username:string;
  password:string;
  
  message:string;
  year = new Date();

  constructor(private repoAuth:AuthService, private router:Router) { }

  ngOnInit() {
  }

  login() {
    this.repoAuth.login(this.username, this.password).subscribe(
      () => this.router.navigate(['']),
      data => this.message = 'Authentication Error.'
    )
  }


}
