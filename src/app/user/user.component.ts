import { Component, OnInit } from '@angular/core';
import { AuthService } from 'app/auth.service';
import { User } from 'app/entity/user';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  user: User;

  constructor(private authService:AuthService) { }

  ngOnInit() {
    this.authService.user.subscribe(user => this.user = user);
  }

}
