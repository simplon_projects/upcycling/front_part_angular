import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap, switchMap } from "rxjs/operators";
import { User } from '../app/entity/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user:BehaviorSubject<User> = new BehaviorSubject(null);
  constructor(private http:HttpClient) {  }

  getToken():string {
    return localStorage.getItem('token');
  }

  login(username:string, password:string) {
    return this.http.post<{token:string}>('http://localhost:8000/api/login_check', {
      username: username,
      password: password
    }).pipe(
      tap(user => localStorage.setItem('token', user.token)),
      switchMap(() => this.getUser())
    );
  }

  logout() {
    localStorage.removeItem('token');
    this.user.next(null);
  }

  getUser(): Observable<User> {
    return this.http.get<User>('http://localhost:8000/api/user').pipe(
      tap(user => this.user.next(user))
    );
  }

  addUser(user:User) {
    return this.http.post<User>('http://localhost:8000/api/user/register', user)
  }

}
