import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpcycledObjectComponent } from './upcycled-object.component';

describe('UpcycledObjectComponent', () => {
  let component: UpcycledObjectComponent;
  let fixture: ComponentFixture<UpcycledObjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpcycledObjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpcycledObjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
