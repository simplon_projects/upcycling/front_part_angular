import { Component, OnInit } from '@angular/core';
import { UpcycledObject } from 'app/entity/upcycledObject';
import { UpcycledObjectService } from 'app/services/upcycled-object.service';

@Component({
  selector: 'app-upcycled-object',
  templateUrl: './upcycled-object.component.html',
  styleUrls: ['./upcycled-object.component.scss']
})
export class UpcycledObjectComponent implements OnInit {

  public upcycledObjects:UpcycledObject[];

  constructor(private repo:UpcycledObjectService) { }

  ngOnInit() { 
    this.repo.getAll().subscribe(
      data => this.upcycledObjects = data
    )
  }

}
