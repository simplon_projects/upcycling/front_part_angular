import { Component, OnInit } from '@angular/core';
import { TrashObjectService } from 'app/services/trash-object.service';
import { TrashObject } from 'app/entity/trashObject';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  trashObject:TrashObject;
  id:any;

  constructor(private repo:TrashObjectService, private route:ActivatedRoute) { }

  ngOnInit() {
    this.id = this.route.snapshot.paramMap.get('id');
    this.repo.getOne(this.id).subscribe(data => this.trashObject = data);
  }

}
