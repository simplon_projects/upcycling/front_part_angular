import { Component, OnInit, NgModule } from '@angular/core';
import { TrashObjectService } from 'app/services/trash-object.service';
import { TrashObject } from 'app/entity/trashObject';

@Component({
  selector: 'app-trash-object',
  templateUrl: './trash-object.component.html',
  styleUrls: ['./trash-object.component.scss']
})
export class TrashObjectComponent implements OnInit {

  public trashObjects:TrashObject[];

  constructor(private repo:TrashObjectService) { }

  ngOnInit() {
    this.repo.getAll().subscribe(
      data => this.trashObjects = data
    )}

}
