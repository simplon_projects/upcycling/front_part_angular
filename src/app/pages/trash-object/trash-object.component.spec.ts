import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrashObjectComponent } from './trash-object.component';

describe('TrashObjectComponent', () => {
  let component: TrashObjectComponent;
  let fixture: ComponentFixture<TrashObjectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrashObjectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrashObjectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
