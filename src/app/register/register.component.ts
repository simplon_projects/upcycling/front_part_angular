import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { User } from 'app/entity/user';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  user:User = {
    email:'',
    password:'',
    username:'',
    lastName:'',
    firstName:'',
    adress:'',
    postalCode:null,
    city:''
  };
  year = new Date();

  @Output()
  registered = new EventEmitter();
  
  constructor(private authService:AuthService) { }

  ngOnInit() {
  }

  register() {
    this.authService.addUser(this.user).subscribe(
      () => this.registered.emit()
    );
  }
}
